clearscreen.
SET rcsTotalThrust TO 0.
LIST RCS IN craftRCS.
for rcs IN craftRCS{
    SET rcsTotalThrust TO (rcsTotalThrust + rcs:maxthrust).
    print rcsTotalThrust.
}
SET rcsTWR TO (rcsTotalThrust / ship:MASS).
PRINT "Vessel Mass is " + ship:MASS.
PRINT "rcsTotalThrust is " + rcsTotalThrust.
PRINT "rcsTWR is " + rcsTWR.
