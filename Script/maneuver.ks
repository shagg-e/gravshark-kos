clearscreen.

function closeToManeuverVec {
    LOCAL diffVec IS (SHIP:FACING:FOREVECTOR:NORMALIZED - NEXTNODE:BURNVECTOR:NORMALIZED).
    return ROUND(diffVec:MAG, 3).
}

function clearLine {
    PARAMETER CharNum.
    PARAMETER LineNum.
    PRINT "                                                               " AT (0,LineNum).
}


function mnv_time {
    parameter dv.
    set ens to list().
    ens:clear.
    set ens_thrust to 0.
    set ens_isp to 0.
    list engines in myengines.

    for en in myengines {
        if en:ignition = true and en:flameout = false {
        ens:add(en).
        }
    }

    for en in ens {
        set ens_thrust to ens_thrust + en:availablethrust.
        set ens_isp to ens_isp + en:isp.
    }

    if ens_thrust = 0 or ens_isp = 0 {
        notify("No engines available!").
        return 0.
    }
    else {
        local f is ens_thrust * 1000.  // engine thrust (kg * m/s²)
        local m is ship:mass * 1000.        // starting mass (kg)
        local e is constant():e.            // base of natural log
        local p is ens_isp/ens:length.               // engine isp (s) support to average different isp values
        local g is ship:orbit:body:mu/ship:obt:body:radius^2.    // gravitational acceleration constant (m/s²)
        return g * m * p * (1 - e^(-dv/(g*p))) / f.
    }
}


LOCK STEERING TO NEXTNODE:BURNVECTOR.
Print "Locking to maneuver node vector." AT (0,0).
PRINT "Difference between current and target vector is " + closeToManeuverVec() AT (0,1).

UNTIL closeToManeuverVec() < 0.05{ 
    PRINT closeToManeuverVec() AT (48,1). //update number printed to console but don't reprint whole line
}

WAIT 10. //pause to give SAS time to lock on
clearLine(0,1).
PRINT "Locked onto maneuver node vector. Timewarping to node." AT (0,1).

SET burnStartTime to (NEXTNODE:TIME - (mnv_time(NEXTNODE:DELTAV:MAG) / 2)). //calc when to start burn
SET warpTarget TO (burnStartTime - 30). //calc 30 seconds before burn start
PRINT "Node is at " + NEXTNODE:TIME + " and burn is at " + warpTarget AT (0,2).
KUNIVERSE:TIMEWARP:WARPTO(warpTarget). //warp to 30sec before node
WAIT 5.
UNTIL SHIP:UNPACKED.
PRINT "Timewarp Complete. Preparing for maneuver!" AT (0,3).

LOCK STEERING TO NEXTNODE:BURNVECTOR. //refresh steering lock
UNTIL TIME:SECONDS >= burnStartTime{
    PRINT "Time until burn: " + ROUND((burnStartTime - TIME:SECONDS), 3) AT (0,4).
} //wait until burn time
PRINT "Here comes the juice! Starting Burn!" AT (0,5).

LOCK THROTTLE TO 1.
UNTIL NEXTNODE:DELTAV:MAG <= 15.
LOCK THROTTLE TO 0.5.
UNTIL NEXTNODE:DELTAV:MAG <= 10.
LOCK THROTTLE TO 0.25.
UNTIL NEXTNODE:DELTAV:MAG <= 5.
LOCK THROTTLE TO 0.15.
UNTIL NEXTNODE:DELTAV:MAG <= 0.2.
LOCK THROTTLE TO 0.
REMOVE NEXTNODE.




WAIT UNTIL RCS.


