CLEARSCREEN.

SET dPos TO TARGET:POSITION.
GLOBAL arrowVec IS TARGET:PORTFACING:FOREVECTOR * 50.
GLOBAL rdyApproachToFinal is FALSE.
SET STEERINGMANAGER:PITCHPID:KD TO 2.
SET STEERINGMANAGER:YAWPID:KD TO 2.
SET STEERINGMANAGER:ROLLPID:KD TO 2.


SET anArrow TO VECDRAW(
        dPos, //start
        arrowVec, //finish
        RGB(1,0,0), //color
        "Final Approach Vector", //label
        1, //scale
        TRUE, //show
        0.1, //width
        TRUE //pointy
    ).

function aimTowardFinalVector {
    updateArrow().
    LOCAL steerVec IS SHIP:FACING:VECTOR.
    SET steerVec TO (TARGET:NODEPOSITION + (TARGET:PORTFACING:FOREVECTOR * 50)).
    LOCK STEERING TO steerVec.
    
    //calculate and print distance to target final
    LOCAL distanceToFinalVector IS (SHIP:POSITION - steerVec).
    SET distanceToFinalVector TO ROUND(distanceToFinalVector:MAG, 5).
    PRINT "Distance to final approach vector: " + distanceToFinalVector AT (0,0).


    SET targetSteerDiff TO closeTargetLock(steerVec, SHIP:FACING:FOREVECTOR).
    PRINT "Difference in steering: " + targetSteerDiff AT (0,1).
    PRINT "Relative velocity to target: " + relVel() AT (0,2).
    Print "Relative XYZ velocity is: (" + relX() + ", " + relY() + ", " + relZ() + ")" AT (0,3).
    if targetSteerDiff > 0.03{
        PRINT "Steering to target vector." AT (0,4).
        aimTowardFinalVector(). //we want to stay in this loop until aimed well
    }
    else if (rdyApproachToFinal = FALSE){
        //clear Steering To  Target Vector text
        clearLine(4).
        PRINT "Steering to target vector complete." AT (0,4).
        SET rdyApproachToFinal TO TRUE.
        RCS ON. // turn this on now that we're exiting the loop and ready to approach
    }



    return distanceToFinalVector.

}

function closeTargetLock {
    PARAMETER targetVec.
    PARAMETER currentFaceVec.

    LOCAL diffVec IS (currentFaceVec:NORMALIZED - targetVec:NORMALIZED).

    return ROUND(diffVec:MAG, 3).
}

function relVel {
    LOCAL tarVel IS TARGET:SHIP:VELOCITY:ORBIT.
    LOCAL shipVel IS SHIP:VELOCITY:ORBIT.
    LOCAL relVelo IS tarVel - shipVel.
    LOCAL relSpeed IS ROUND(relVelo:MAG, 3).

    return relSpeed.

}

function relX{
    LOCAL tarX IS TARGET:SHIP:VELOCITY:ORBIT:X.
    LOCAL shipX IS SHIP:VELOCITY:ORBIT:X.
    LOCAL relVelX IS tarX - shipX.
    SET relVelX TO ROUND(relVelX, 6).

    return relVelX.
}

function relY{
    LOCAL tarY IS TARGET:SHIP:VELOCITY:ORBIT:Y.
    LOCAL shipY IS SHIP:VELOCITY:ORBIT:Y.
    LOCAL relVelY IS tarY - shipY.
    SET relVelY TO ROUND(relVelY, 6).

    return relVelY.
}

function relZ{
    LOCAL tarZ IS TARGET:SHIP:VELOCITY:ORBIT:Z.
    LOCAL shipZ IS SHIP:VELOCITY:ORBIT:Z.
    LOCAL relVelZ IS tarZ - shipZ.
    SET relVelZ TO ROUND(relVelZ, 6).

    return relVelZ.
}
function updateArrow {
    SET dPos TO TARGET:POSITION.
    SET arrowVec TO TARGET:PORTFACING:FOREVECTOR * 50.
    SET anArrow:START TO dPos.
    SET anArrow:VEC TO arrowVec.
}

function clearLine{
    PARAMETER line2Clear.
    PRINT "                                                            " AT (0, line2Clear).
}

function relStarboard{
    SET relVec TO SHIP:VELOCITY:ORBIT - TARGET:SHIP:VELOCITY:ORBIT.
    SET starboardRelVel TO ROUND((SHIP:FACING:STARVECTOR * relVec), 4).

    return starboardRelVel.
}

function relFore{
    SET relVec TO SHIP:VELOCITY:ORBIT - TARGET:SHIP:VELOCITY:ORBIT.
    SET foreRelVel TO ROUND((SHIP:FACING:FOREVECTOR * relVec), 4).

    return foreRelVel.
}

function relTop{
    SET relVec TO SHIP:VELOCITY:ORBIT - TARGET:SHIP:VELOCITY:ORBIT.
    SET topRelVel TO ROUND((SHIP:FACING:TOPVECTOR * relVec), 4).

    return topRelVel.
}

function printRel{
    clearLine(6).
    PRINT "Relative XYZ to prograde: (" + relStarboard() + ", " + 
        relFore() + ", " + relTop() + ")" AT (0,6). 
}

function translateRCS{
    PARAMETER isApproachingFinal.

    UNTIL ABS(relStarboard()) < 0.005{
        if isApproachingFinal {aimTowardFinalVector().}
        IF relStarboard() < 0{
            printRel().
            SET SHIP:CONTROL:STARBOARD TO 1.
        }
        ELSE IF relStarboard() > 0{
            printRel().
            SET SHIP:CONTROL:STARBOARD TO -1.
        }
    }
    SET SHIP:CONTROL:STARBOARD TO 0.
    UNTIL ABS(relTop()) < 0.005{
        if isApproachingFinal {aimTowardFinalVector().}
        IF relTop() < 0{
            printRel().
            SET SHIP:CONTROL:TOP TO 1.
        }
        ELSE IF relTop() > 0{
            printRel().
            SET SHIP:CONTROL:TOP TO -1.
        }
    }
    SET SHIP:CONTROL:TOP TO 0.

}

RCS ON. // enable RCS to start pointing to final vector

UNTIL BRAKES{ // use BRAKES as a way to just force the loop

    UNTIL aimTowardFinalVector() < 50{
        //Get us up to 1 m/s rel to the target
        UNTIL relVel() > 1{
            aimTowardFinalVector(). //check we are aiming good still
            PRINT "Firing forward to start approach toward final vector." AT (0,5).
            SET SHIP:CONTROL:FORE TO 1.
        }
        PRINT "                                                          " AT (0,5).
        PRINT "Cutting forward thrusters. Holding speed and heading." AT (0,5).
        SET SHIP:CONTROL:FORE TO 0. //stop firing forward RCS
        translateRCS(TRUE).
    }

    UNTIL aimTowardFinalVector() < 25{
        UNTIL relVel() < 0.5{
            aimTowardFinalVector().
            translateRCS(TRUE).
            PRINT "Slowing to 0.5 m/s" AT (0, 7).
            SET SHIP:CONTROL:FORE TO -1.
        }
        clearLine(7).
        SET SHIP:CONTROL:FORE TO 0.
        translateRCS(TRUE).
    }

    UNTIL aimTowardFinalVector() < 10{
        UNTIL relVel() < 0.4{
            aimTowardFinalVector().
            translateRCS(TRUE).
            PRINT "Slowing to 0.4 m/s" AT (0, 7).
            SET SHIP:CONTROL:FORE TO -1.
        }
        clearLine(7).
        SET SHIP:CONTROL:FORE TO 0.
        translateRCS(TRUE).
    }
    
    UNTIL aimTowardFinalVector() < 5{
        UNTIL relVel() < 0.3{
            aimTowardFinalVector().
            translateRCS(TRUE).
            PRINT "Slowing to 0.3 m/s" AT (0, 7).
            SET SHIP:CONTROL:FORE TO -1.
        }
        clearLine(7).
        SET SHIP:CONTROL:FORE TO 0.
        translateRCS(TRUE).
    }

    UNTIL aimTowardFinalVector() < 1{
        UNTIL relVel() < 0.15{
            aimTowardFinalVector().
            translateRCS(TRUE).
            PRINT "Slowing to 0.15 m/s" AT (0, 7).
            SET SHIP:CONTROL:FORE TO -1.
        }
        clearLine(7).
        SET SHIP:CONTROL:FORE TO 0.
    }

    UNTIL relVel() < 0.01{
            aimTowardFinalVector().
            translateRCS(TRUE).
            PRINT "Stopping" AT (0, 7).
            SET SHIP:CONTROL:FORE TO -1.
    }
    SET SHIP:CONTROL:FORE TO 0.

    //now we aim to the target port and start moving in
    clearVecDraws().
    clearscreen.
    UNLOCK STEERING.
    PRINT "Final vector approached. Aiming to target to begin terminal approach." AT (0,0).
    SET targetSteerDiff TO closeTargetLock(TARGET:PORTFACING:FOREVECTOR, SHIP:FACING:FOREVECTOR).
    LOCK STEERING TO TARGET:POSITION.
    UNTIL targetSteerDiff < 0.01{
        SET targetSteerDiff TO closeTargetLock((TARGET:PORTFACING:FOREVECTOR * -1), SHIP:FACING:FOREVECTOR).
    }
    PRINT "Pausing 15 seconds before beginning terminal approach." AT (0,1).
    WAIT 15.
    BREAK.
}


//TERMINAL APPROACH
UNTIL BRAKES{
    LOCK STEERING TO TARGET:POSITION.
    UNTIL relVel() > 1{
        translateRCS(FALSE).
        PRINT "Beginning Approach at 1 m/s" AT (0, 2).
        SET SHIP:CONTROL:FORE TO 1.
        }
    SET SHIP:CONTROL:FORE TO 0.
    IF TARGET:STATE = "Ready"{
        UNTIL (TARGET:POSITION - SHIP:ROOTPART:POSITION):MAG < 25{
            translateRCS(FALSE).
        }
        UNTIL (TARGET:POSITION - SHIP:ROOTPART:POSITION):MAG < 10{
            UNTIL relVel() < 0.5{
                translateRCS(FALSE).
                SET SHIP:CONTROL:FORE TO -1.
            }
            SET SHIP:CONTROL:FORE TO 0.
            translateRCS(FALSE).
        }
        UNTIL (TARGET:POSITION - SHIP:ROOTPART:POSITION):MAG < 5{
            UNTIL relVel() < 0.2{
                translateRCS(FALSE).
                SET SHIP:CONTROL:FORE TO -1.
            }
            SET SHIP:CONTROL:FORE TO 0.
            translateRCS(FALSE).
        }
        UNTIL TARGET:STATE = "Docked"{
            translateRCS(FALSE).
        }
    }
    ELSE{
        BREAK.
    }
}
clearVecDraws().
CLEARSCREEN.